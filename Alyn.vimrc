" Disable compatibillity with vi which can cause unexpected issues.
set nocompatible

filetype off
"set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'itchyny/lightline.vim'
Plugin 'dracula/vim',{'name':'dracula'}

"All Plugins must be addd before the following line
call vundle#end()


filetype plugin indent on

" Enable type file detection. Vim will be able to try to detect the type of
"file in use.
filetype on

" Enable plugings and load plugin for the detected file type.
filetype plugin on

" Load an indent file for the detected file type.
filetype indent on


" Show (partial) command in the last line of the screen 
set showcmd


" Turn syntax highlighting on.
syntax on

" Select font
if has("gui_gtk")
   set guifont=JetBrains\ Mono\ NL\ Medium\ 11
elseif has("win32") || has("win64")
   set guifont=JetBrains_Mono_Medium:h9:cRUSSIAN:qDRAFT
endif


" Load colorscheme
" colorscheme teledark 
" colorscheme cobalt-Alyn
colorscheme dracula

" Lightline coloscheme
" let g:lightline = {'colorscheme': 'wombat'}
" Lightline enabled, so hide mode
set noshowmode

" Add numbers to each line on the left-hand side.
set number

" Highlight column 80
set colorcolumn=80

" Highlight cursor line underneath the cursor horizontally.
set cursorline

" Set shift width to 4 spaces.
set shiftwidth=4

" Set tab width to 4 columns.
set tabstop=4
set expandtab

" Ignore capital letters during search.
set ignorecase

" Override the ignorecase option if searching for capital letters.
" This will allow to search specifically for capital letters.
set smartcase

" Switch off gui-window toolbar
set guioptions-=T

" Display only filename on tabs
set guitablabel=%t

set laststatus=2

"if !has('gui_running')
"    set t_Co=256
"endif

if (has("termguicolors"))
    set termguicolors
endif


