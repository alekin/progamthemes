# ProgramThemes
Color schemes and config files

## VIM
cobalt-Alyn.vim - cobalt based color-theme.
Windows: place into `$HOME\vimfiles\colors\` directory
Linux: place into $HOME\.vim\colors\

Alyn.gvimrc - Configuration file for VIM.
Windows: place as `$HOME\vimfiles\gvimrc` file
Linux: place as $HOME\.vim\gvimrc

## NVIM
Install vim-plug from 
https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim  
to
 * win: ~/AppData/Local/nvim-data/site/autoload/plug.vim
 * nix: ~/.config/nvim/autoload/plug.vim


use :PlugInstall command at first nvim run


Place init.vim to:
 * win: ~/AppData/Local/nvim/init.vim
 * nix: ~/.config/nvim/init.vim 



## gedit
cobalt-Alyn-GeditTheme.xml - cobalt based color-theme
to install theme: goto preferences -> Font & Colors -> [+] (Install theme)
 
## Notepad++
cobalt-Alyn-NotepadPP.xml - cobalt based color-theme
