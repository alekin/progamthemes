" nvim config
" win: ~/AppData/Local/nvim/init.vim
" nix: 

" Настраиваем переключение раскладок клавиатуры по <C-^> ("CTL"+"6")
set keymap=russian-jcukenwin

" Раскладка по умолчанию - английская
set iminsert=0

" аналогично для строки поиска и ввода команд
set imsearch=0

" vim-plug section
" to install vim-plug copy file plug.vim from
" https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim  
" to
" win: ~/AppData/Local/nvim-data/site/autoload/plug.vim
" nix: /nvim/site/autoload/plug.vim
" use :PlugInstall command at first nvim run
call plug#begin()
Plug 'itchyny/lightline.vim'
Plug 'dracula/vim', { 'as': 'dracula' }
call plug#end()

" Set tab width to 4 columns.
set expandtab
set shiftwidth=4
set tabstop=4
colorscheme dracula
set number
set noshowmode
set showcmd
set colorcolumn=80

" Highlight cursor line underneath the cursor horizontally.
set cursorline


set laststatus=2

if (has("termguicolors"))
    set termguicolors
endif

function! LightLineKeymap()
    return &iminsert == 0 ? 'EN' : 'RU' 
endfunction

let g:lightline = {
	\'colorscheme': 'dracula',
    \'active': {
    \   'right': [ [ 'lineinfo' ],
    \              [ 'percent' ],
    \              [ 'fileformat', 'fileencoding', 'filetype', 'keymap' ] ]
    \ }, 
    \'component_function': {
    \    'keymap': 'LightLineKeymap',
    \    },
	\}


